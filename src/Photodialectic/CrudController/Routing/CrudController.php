<?php namespace Photodialectic\CrudController\Routing;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Routing\Controller;
use Input;
use Lang;
use Redirect;
use Request;
use Response;
use Route;
use Session;
use Validator;

abstract class CrudController extends Controller
{

	const ACTION_CREATE = 1;
	const ACTION_UPDATE = 2;
	const ACTION_SHOW   = 3;

	/**
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * Cache the model instance
	 *
	 * @var
	 */
	private $instance;

	/**
	 * Get variables from route
	 *
	 * @return array
	 */
	public function getRouteVariables()
	{
		return Route::getCurrentRoute()->parameters();
	}

	/**
	 * Set the model instance based on route bindings and specified model
	 *
	 * @return \Illuminate\Database\Eloquent\Model
	 * @throws \BadMethodCallException when the current route does not have an instance of its model
	 */
	public function getModelInstance()
	{
		if (null === $this->instance)
		{
			// Find and cache the instance for the current model
			foreach (Route::current()->parameters() as $v)
			{
				if ($v instanceof $this->model)
				{
					$this->instance = $v;
					return $this->instance;
				}
			}
			throw new \BadMethodCallException("Could not find '{$this->model}' model in the route");
		}
		else
		{
			// Return the cached route
			return $this->instance;
		}
	}

	/**
	 * Fetch a list of variables that should be passed to the view for create, store, edit, and update.
	 *
	 * @param \Illuminate\Database\Eloquent\Model $instance Current instance of the model
	 * @param int|string $mode Add or edit
	 * @return array Key/Value pairs to be passed to the view
	 * @codeCoverageIgnore
	 */
	protected function getAddEditVariables(Eloquent $instance, $mode = self::ACTION_CREATE)
	{
		return array();
	}

	/**
	 * In store and update, filter the variables that are passed from the user. This
	 * is primarily useful to set the user's input to what they were expected to
	 * input, especially for hidden or readonly fields.
	 *
	 * Overrides should be set with Input::getFacadeRoot()->request->add()
	 * @codeCoverageIgnore
	 */
	protected function filterPreValidation(Eloquent $instance, $viewVariables, $mode = self::ACTION_CREATE)
	{
	}

	/**
	 * In store and update, return the list of validation rules used in Validator::make()
	 *
	 * @see Illuminate\Validation\Factory::make
	 * @param            $instance
	 * @param            $viewVariables
	 * @param int|string $mode
	 * @return array
	 * @codeCoverageIgnore
	 */
	protected function getValidationRules(Eloquent $instance, $viewVariables, $mode = self::ACTION_CREATE)
	{
		return array();
	}

	/**
	 * In store and update, save the Eloquent model
	 *
	 * @param \Illuminate\Database\Eloquent\Model $instance
	 * @param int|string $mode add or edit
	 * @return boolean                                      Whether or not the save worked
	 * @codeCoverageIgnore
	 */
	public function saveModel(Eloquent $instance, $mode = self::ACTION_CREATE)
	{
		return false;
	}

	/**
	 * Determine where to redirect to after special POSTy actions
	 *
	 * @param \Illuminate\Database\Eloquent\Model $instance
	 * @param $mode
	 * @return \Illuminate\Http\Response
	 */
	public function determineRedirect(Eloquent $instance, $mode)
	{
		switch ($mode)
		{
			case self::ACTION_CREATE:
			case self::ACTION_UPDATE:
			default:
				return $this->redirectToAction('show', $instance->getKey());
				break;
		}
	}

	/**
	 * Redirect to the requested action for the current route
	 *
	 * @param      $action string
	 * @param null $id integer
	 * @throws Exception
	 * @return Illuminate\Http\Response
	 */
	public function redirectToAction($action, $id = null)
	{
		// Get the Controller@action name for the destination we want
		list($currentController) = explode('@', Route::currentRouteAction());
		if (null === ($destinationRoute = Route::getRoutes()->getByAction("$currentController@$action")))
		{
			throw new Exception("Action {$action} does not exist in {$currentController}");
		}

		// Figure out what the destination parameters should be
		$destinationParameters = array();
		foreach ($destinationRoute->parameterNames() as $requiredParameter)
		{
			// We assume the parameter(s) we don't have in the current route is the new object we created
			$value = Route::current()->parameter($requiredParameter, $id);
			if ($value instanceof Eloquent)
			{
				$value = $value->getKey();
			}
			$destinationParameters[$requiredParameter] = $value;
		}

		return Redirect::route($destinationRoute->getName(), $destinationParameters);
	}

	/**
	 * Create or update an instance of the model
	 *
	 * @param \Illuminate\Database\Eloquent\Model $instance
	 * @param                                    $mode
	 * @return Illuminate\Http\Response
	 */
	public function createOrEdit(Eloquent $instance, $mode)
	{
		$viewVariables = $this->getAddEditVariables($instance, $mode) + array('instance' => $instance);

		if (Request::ajax())
		{
			return Response::json(
				array_map(function ($row)
				{
					if ($row instanceof ArrayableInterface)
					{
						return $row->toArray();
					}
					else
					{
						return $row;
					}
				}, $viewVariables),
				200,
				array(
					'Cache-Control' => 'no-cache',
					'Pragma'        => 'no-cache',
				)
			);
		}

		return Response::view(str_replace(array('{', '}',), array('', ''), Route::currentRouteName()), $viewVariables)
			->header('Cache-Control', 'no-cache')
			->header('Pragma', 'no-cache');
	}

	/**
	 * Store or update a newly created resource in storage.
	 *
	 * @param \Illuminate\Database\Eloquent\Model $instance
	 * @param                                    $mode
	 * @return Response
	 */
	public function storeOrUpdate(Eloquent $instance, $mode)
	{
		if (Input::get('cancel') !== null)
		{
			switch ($mode)
			{
				case self::ACTION_CREATE:
				default:
					return $this->redirectToAction('index');
					break;
				case self::ACTION_UPDATE:
					return $this->redirectToAction('show', $instance->getKey());
					break;
			}
		}

		// Handle pre-validation filters
		$viewVariables = $this->getAddEditVariables($instance, $mode);
		$this->filterPreValidation($instance, $viewVariables, $mode);

		// Handle validation
		$validator = Validator::make(Input::all(), $this->getValidationRules($instance, $viewVariables, $mode));
		if ($validator->passes())
		{
			// Persist the changes
			$status = $this->saveModel($instance, $mode);
			// Allow the saveModel to return a response if needed
			if ($status instanceof \Symfony\Component\HttpFoundation\Response)
			{
				return $status;
			}
		}
		else
		{
			$status = false;
		}

		// Pass errors to the browser
		if (!$status)
		{
			if (Request::ajax())
			{
				return Response::json(
					array(
						'success' => false,
						'header'  => Lang::get('models.' . strtolower($this->model) . '_save_failure_title', array_dot($instance->attributesToArray())),
						'message' => Lang::get('models.' . strtolower($this->model) . '_save_failure_body', array_dot($instance->attributesToArray())),
					),
					200,
					array(
						'Cache-Control' => 'no-cache',
						'Pragma'        => 'no-cache',
					)
				);
			}
			else
			{
				Input::flash();

				Session::flash('flash_message', array(
					'severity' => 'danger',
					'header'   => Lang::get('models.' . strtolower($this->model) . '_save_failure_title', array_dot($instance->attributesToArray())),
					'message'  => Lang::get('models.' . strtolower($this->model) . '_save_failure_body', array_dot($instance->attributesToArray())),
				));

				// Redirect back to the create page
				switch ($mode)
				{
					case self::ACTION_CREATE:
					default:
						return $this->redirectToAction('create')->withErrors($validator);
						break;
					case self::ACTION_UPDATE:
						return $this->redirectToAction('edit', $instance->getKey())->withErrors($validator);
						break;
				}
			}
		}

		// Issue the success
		if (Request::ajax())
		{
			return Response::json(
				array(
					'success'     => true,
					'instance_id' => $instance->getKey(),
				),
				200,
				array(
					'Cache-Control' => 'no-cache',
					'Pragma'        => 'no-cache',
				)
			);
		}

		$label = $mode === self::ACTION_UPDATE ? '_updated' : '_created';

		Session::flash('flash_message', array(
			'severity' => 'success',
			'header'   => Lang::get('models.' . strtolower($this->model) . $label . '_title', array_dot($instance->attributesToArray())),
			'message'  => Lang::get('models.' . strtolower($this->model) . $label . '_body', array_dot($instance->attributesToArray())),
		));

		// Redirect back to the show page
		return $this->determineRedirect($instance, $mode);
	}

	/**
	 * Default action (list)
	 *
	 * @return Illuminate\Http\Response
	 * @throws BadMethodCallException when model not defined
	 */
	public function index()
	{
		// Figure out what data we're working with
		$model = new $this->model;
		$query = $model->query();

		// Now handle custom search parameters
		foreach (Input::all() as $key => $value)
		{
			if (substr($key, 0, 7) === 'search_')
			{
				$query->where(
					$model->getModel()->getTable() . '.' . substr($key, 7),
					'like',
					$value
				);
			}
		}

		// Build out our paginator now that we have all the data
		$paginator = $query->paginate(null, array($model->getModel()->getTable() . '.*'));

		// Return special request for AJAX
		if (Request::ajax())
		{
			return Response::json(array(
				'current_page' => $paginator->getCurrentPage(),
				'last_page'    => $paginator->getLastPage(),
				'total'        => $paginator->getTotal(),
				'results'      => $paginator->getCollection()->toArray(),
			), 200, array(
				'Cache-Control' => 'no-cache',
				'Pragma'        => 'no-cache',
			));
		}

		// Return the default view
		return
			Response::view(
				str_replace(array('{', '}',), array('', ''), Route::currentRouteName()),
				array(
					'model'     => $model,
					'paginator' => $paginator,
				)
			)
				->header('Cache-Control', 'no-cache')
				->header('Pragma', 'no-cache');
	}

	/**
	 * View a single instance
	 *
	 * @internal param \Illuminate\Database\Eloquent\Model $instance
	 * @return Illuminate\Http\Response
	 */
	public function show()
	{
		// Figure out what data we're working with
		$model = new $this->model;

		$instance = $this->getModelInstance();

		// Return special request for AJAX
		if (Request::ajax())
		{
			return Response::json(
				$instance->toArray(),
				200,
				array(
					'Cache-Control' => 'no-cache',
					'Pragma'        => 'no-cache',
				));
		}

		$viewVariables = $this->getAddEditVariables($instance, self::ACTION_SHOW);

		return Response::view(
			str_replace(array('{', '}',), array('', ''), Route::currentRouteName()),
			$viewVariables + array(
				'model'    => $model,
				'instance' => $instance,
			)
		)
			->header('Cache-Control', 'no-cache');
	}

	/**
	 * Create an instance of the model
	 *
	 * @return Illuminate\Http\Response
	 */
	public function create()
	{
		$instance = new $this->model;
		return $this->createOrEdit($instance, self::ACTION_CREATE);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$instance = new $this->model();
		return $this->storeOrUpdate($instance, self::ACTION_CREATE);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @internal param \Illuminate\Database\Eloquent\Model $instance
	 * @return Response
	 */
	public function edit()
	{
		$instance = $this->getModelInstance();
		return $this->createOrEdit($instance, self::ACTION_UPDATE);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @internal param \Illuminate\Database\Eloquent\Model $instance
	 * @return Response
	 */
	public function update()
	{
		$instance = $this->getModelInstance();
		return $this->storeOrUpdate($instance, self::ACTION_UPDATE);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @internal param \StarTribune\CrudController\Routing\Illuminate\Database\Eloquent\Model $instance
	 * @return Response
	 */
	public function destroy()
	{
		//Get the instance
		$instance = $this->getModelInstance();

		// Issue the delete
		$instance->delete();

		// Notify the end user
		if (Request::ajax())
		{
			return Response::json(array(
				'success' => true,
			));
		}

		Session::flash('flash_message', array(
			'severity' => 'success',
			'header'   => Lang::get('models.' . strtolower($this->model) . '_deleted_title', array_dot($instance->attributesToArray())),
			'message'  => Lang::get('models.' . strtolower($this->model) . '_deleted_body', array_dot($instance->attributesToArray())),
		));

		// Redirect back to the index page
		return $this->redirectToAction('index');
	}

}