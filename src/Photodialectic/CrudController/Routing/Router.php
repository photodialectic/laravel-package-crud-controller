<?php namespace Photodialectic\CrudController\Routing;

use Illuminate\Routing\Router as LaravelRouter;
use \Closure;

class Router extends LaravelRouter
{

	/**
	 * Register a model binder for a wildcard.
	 *
	 * @param  string  $key
	 * @param  string  $class
	 * @param  \Closure  $callback
	 * @return void
	 *
	 * @throws NotFoundHttpException
	 */
	public function model($key, $class, Closure $callback = null)
	{
		return $this->bind($key, function($value) use ($class, $callback)
		{
			if (is_null($value)) return null;

			$model = new $class;
			$query = $model->query();

			// See if we want to eager load any data
			if (method_exists($query->getModel(), 'scopeEagerLoad'))
			{
				$query->eagerLoad();
			}

			// Find by primary key
			$query->where($model->getModel()->getTable() . '.' . $model->getModel()->getKeyName(), '=', $value);

			// For model binders, we will attempt to retrieve the models using the find
			// method on the model instance. If we cannot retrieve the models we'll
			// throw a not found exception otherwise we will return the instance.
			if ( ! is_null($model = $query->first(array($model->getModel()->getTable() . '.*'))))
			{
				return $model;
			}

			// If a callback was supplied to the method we will call that to determine
			// what we should do when the model is not found. This just gives these
			// developer a little greater flexibility to decide what will happen.
			if ($callback instanceof Closure)
			{
				return call_user_func($callback);
			}

			throw new NotFoundHttpException;
		});
	}

}
